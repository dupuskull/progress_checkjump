﻿using System;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data.Models;

namespace checkjump.Data.Database
{
	public class CheckJumpDatabase
	{
		SQLiteConnection database;
		public CheckJumpDatabase ()
		{
			database = DependencyService.Get<ISqlite> ().GetConnection ();
			if (database.TableMappings.All(t => t.MappedType.Name != typeof(Product).Name)) {
				database.CreateTable<Product> ();
				database.Commit ();
			}
			if (database.TableMappings.All(t => t.MappedType.Name != typeof(User).Name)) {
				database.CreateTable<User> ();
				database.Commit ();
			}
		}

		public List<Product> GetAllProducts(){
			var items = database.Table<Product> ().ToList<Product>();

			return items;
		}

		public int InsertOrUpdateProduct(Product product){
			return database.Table<Product> ().Where (x => x.ProductName == product.ProductName).Count () > 0 
				? database.Update (product) : database.Insert (product);
		}

		public Product GetProduct(string key){
			return database.Table<Product> ().First (t => t.ProductName == key); 
		}

		public List<User> GetAllUsers(){
			var items = database.Table<User> ().ToList<User>();

			return items;
		}

		public int InsertOrUpdateUser(User user){
			return database.Table<User> ().Where (x => x.Email == user.Email).Count () > 0 
				? database.Update (user) : database.Insert (user);
		}

		public User GetUser(string key){
			return database.Table<User> ().First (t => t.Email == key); 
		}

	}
}


