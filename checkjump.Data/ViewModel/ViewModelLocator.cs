/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:checkjump.Data"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using checkjump.Data;
using checkjump.Data.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace checkjump.Data.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
		public const string productlistPageKey = "productlist";
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            ////if (ViewModelBase.IsInDesignModeStatic)
            ////{
            ////    // Create design time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            ////}
            ////else
            ////{
            ////    // Create run time view services and models
            ////    SimpleIoc.Default.Register<IDataService, DataService>();
            ////}

            //SimpleIoc.Default.Register<MainViewModel>();
//			SimpleIoc.Default.Register<ProductCategoryViewModel>(() => 
//				{
//					return new ProductCategoryViewModel(
//						SimpleIoc.Default.GetInstance<IMyNavigationService>()
//					);
//				});
			SimpleIoc.Default.Register<ProductCategoryViewModel>(() => 
				{
					return new ProductCategoryViewModel(
					);
				});
		}

//        public MainViewModel Main
//        {
//            get
//            {
//                return ServiceLocator.Current.GetInstance<MainViewModel>();
//            }
//        }

	public ProductCategoryViewModel products
	{
		get
		{
			return ServiceLocator.Current.GetInstance<ProductCategoryViewModel>();
		}
	}

	

//	public ProductViewModel productlist
//	{
//		get
//		{
//			return ServiceLocator.Current.GetInstance<ProductViewModel> ();
//		}
//	}
        
        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}