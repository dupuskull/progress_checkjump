﻿using System;
using System.Windows.Input;
using checkjump.Data.Models;
using Xamarin.Forms;

namespace checkjump.Data.ViewModel
{
	public class ProductCategoryViewModel
	{
		private IMyNavigationService navigationService;

		public ICommand ProductListViewCommand { get; set; }
		public ICommand SelectProductCommand { get; private set; }

		public ProductCategoryViewModel ()
		{
			ProductListViewCommand = new Command (ProductListView);
			SelectProductCommand = new Command (x => {
				// based on which image is selected, different db queries will be performed before posting to the product list view page
//				switch (x) {
//				case "FreshFood":
//					break;
////				case ""
//				default:
//				break;
//				}
			});
		}

		public void ProductListView()
		{
			this.navigationService.NavigateTo (ViewModelLocator.productlistPageKey);
		}
	}
}

