﻿using System;
using System.Collections.ObjectModel;
using checkjump.Data.Models;
using System.Windows.Input;
using Xamarin.Forms;


namespace checkjump.Data.ViewModel
{
	public class ProductViewModel
	{

        public ObservableCollection<Product> Products { get; set; }

        public string ProductName { get; set; }
		public ProductViewModel()
		{
			
			ProductName = "Product 1 From MVVM";
            Products = new ObservableCollection<Product>
            {
                new Product ("Product 1", "apple.jpg", "$12.50"), 
                new Product("Product 2", "apple.jpg", "$1"),
                new Product ("Product 3", "apple.jpg", "$12.50"),
                new Product("Product 4", "apple.jpg", "$1"),
				new Product ("Product 5", "apple.jpg", "$12.50"), 
				new Product("Product 6", "apple.jpg", "$1"),
				new Product ("Product 7", "apple.jpg", "$12.50"),
				new Product("Product 8", "apple.jpg", "$1")
            };
		}
	}
}

