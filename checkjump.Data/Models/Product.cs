﻿using System;

namespace checkjump.Data.Models
{
	public class Product
	{
		public string ProductName { get; set; }
		public string Image { get; set; }
		public string Price { get; set; }

		public Product (string productName, string image, string price)
		{
			ProductName = productName;
			Image = image;
			Price = price;
		}
	}
}

