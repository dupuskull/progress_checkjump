﻿using System;

namespace checkjump.Data.Models
{
	public class Item
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public int Allergy { get; set; }

		public char Aisle { get; set; }
		public double RRP { get; set; }
		public double SalePrice { get; set; }
		public int Quantity { get; set; }
		public int Weight { get; set; }

		public Item(int Id, string Name, string Image, int Allergy, char Aisle, double RRP, double SalePrice, int Quantity, int Weight)
		{
			this.Id = Id;
			this.Name = Name;
			this.Image = Image;
			this.Allergy = Allergy;
			this.Aisle = Aisle;
			this.RRP = RRP;
			this.SalePrice = SalePrice;
			this.Quantity = Quantity;
			this.Weight = Weight;
		}
	}
}
