﻿using System;

namespace checkjump.Data.Models
{
	public class User
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string BirthDate { get; set; }
		public int PostCode { get; set; }
		public int RewardsCardNumber { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public string ConfirmPassword { get; set; }

		public User (
			string firstname, string lastname, string birthdate, 
			int postcode, int rewardscardnumber, string email, 
			string password, string confirmpassword)
		{
			FirstName = firstname;
			LastName = lastname;
			BirthDate = birthdate;
			PostCode = postcode;
			RewardsCardNumber = rewardscardnumber;
			Email = email;
			Password = password;
			ConfirmPassword = confirmpassword;
		}
	}
}

