﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using checkjump.Views;
using Xamarin.Forms;
using checkjump.Data.ViewModel;
using GalaSoft.MvvmLight.Ioc;
using checkjump.Data;

namespace checkjump
{
	public class App : Application
	{
		public static ViewModelLocator _locator;
		private static NavigationService nav;
		public static ViewModelLocator Locator
		{
			get 
			{
				return _locator ?? (_locator = new ViewModelLocator ());
			}
		}

		public App ()
		{
			// The root page of your application
			MainPage = new login();
			//MainPage = new productlist();
			//MainPage = new products();
//			MainPage = new signUp();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}

