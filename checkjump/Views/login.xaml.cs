﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class login : ContentPage
	{
		public login()
		{
			InitializeComponent();
		}


		private void signinButton_OnClicked(object sender, EventArgs e)
		{

			Navigation.PushModalAsync(new NavigationPage(new navigationC()));

		}

		private void signUpButton_OnClicked(object sender, EventArgs e)
		{
			Navigation.PushModalAsync(new signUp());
		}
	}
}
