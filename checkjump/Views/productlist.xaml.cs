﻿using System;
using System.Collections.Generic;
using checkjump.Data;
using checkjump.Data.ViewModel;
using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class productlist : ContentPage
	{
		public productlist ()
		{
			InitializeComponent ();
			this.BindingContext = new ProductViewModel ();
		}
	}
}

