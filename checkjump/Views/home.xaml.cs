﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Data.Models;
using checkjump.Data.ViewModel;
using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class home : ContentPage
	{
		public home()
		{
			InitializeComponent();

			//add item to the list
			var itemList = new List<Item>();
			itemList.Add(new Item(1, "Oreo Double Choc", "mushroom.png", 0, 'I', 2.50, 1.50, 1, 150));
			itemList.Add(new Item(2, "Hand Towel x 3", "mushroom.png", 0, 'J', 4.00, 3.00, 1, 200));
			itemList.Add(new Item(3, "Oreo Cream", "mushroom.png", 0, 'I', 2.50, 1.50, 1, 150));
			itemList.Add(new Item(4, "Smith Sour Cream", "mushroom.png", 0, 'H', 3.00, 1.75, 3, 175));
			itemList.Add(new Item(5, "Ovantine 500g", "mushroom.png", 0, 'I', 8.00, 6.00, 1, 500));
			itemList.Add(new Item(6, "Peanut Butter", "mushroom.png", 1, 'A', 3.50, 3.00, 1, 300));
			itemList.Add(new Item(7, "Frozen Pizza", "mushroom.png", 0, 'O', 4.50, 4.00, 2, 250));
			itemList.Add(new Item(8, "Hand Soap", "mushroom.png", 0, 'K', 2.50, 1.50, 1, 150));
			itemList.Add(new Item(9, "Hand Cream", "mushroom.png", 0, 'K', 8.00, 6.50, 1, 500));

			//create a ListView to display the item list using ItemCell format
			var shoppinglist = new ListView
			{
				HasUnevenRows = true,
				ItemTemplate = new DataTemplate(typeof(ItemCell)),
				ItemsSource = itemList
			};

			var myShopping = new Label();
			myShopping.Text = "My List";

			var recipes = new Label();
			recipes.Text = "Recommended Recipes";

			//Stack all the view
			var layout = new StackLayout
			{
				Children = {
					myShopping,
					shoppinglist,
					recipes
				}
			};


			//Show the layout to the ContentPage
			this.Content = layout;
			this.Content.BackgroundColor = Color.White;
		}
	}
}
