﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace checkjump.Views
{
	public partial class navigationC : TabbedPage
	{
		public navigationC()
		{
			Title = "woolworths";

			Children.Add(new home());
			Children.Add(new products());
			Children.Add(new checkJump());
			Children.Add(new virtualCart());

			ToolbarItem magnify = new ToolbarItem();
			magnify.Text = "Search";

			ToolbarItem account = new ToolbarItem();
			account.Text = "Account";

			this.ToolbarItems.Add(magnify);
			this.ToolbarItems.Add(account);
		}
	}
}
