﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using checkjump.Views;
using Xamarin.Forms;
using checkjump.Data.ViewModel;

namespace checkjump.Views
{
	public partial class products : ContentPage
	{
		public products()
		{
			InitializeComponent();
			BindingContext = App.Locator.products;
		//	this.BindingContext = new ProductCategoryViewModel ();
		}

		private void category_onclicked(object sender, EventArgs e)
		{

			Navigation.PushAsync (new productlist());

		}
	}
}
